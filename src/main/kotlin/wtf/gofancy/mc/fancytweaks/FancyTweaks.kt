package wtf.gofancy.mc.fancytweaks

import net.minecraft.client.gui.ScreenManager
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent
import thedarkcolour.kotlinforforge.forge.MOD_BUS
import wtf.gofancy.mc.fancytweaks.client.feature.chest.RemoteChestScreen
import wtf.gofancy.mc.fancytweaks.common.BlockEntities
import wtf.gofancy.mc.fancytweaks.common.Blocks
import wtf.gofancy.mc.fancytweaks.common.Containers
import wtf.gofancy.mc.fancytweaks.common.Items
import wtf.gofancy.mc.fancytweaks.datagen.BlockStates
import wtf.gofancy.mc.fancytweaks.datagen.LanguageEnglishUS

@Mod(MOD_ID)
internal object FancyTweaks {

    init {
        Blocks.register(MOD_BUS)
        Items.register(MOD_BUS)
        BlockEntities.register(MOD_BUS)
        Containers.register(MOD_BUS)

        MOD_BUS.addListener(this::clientSetup)
        MOD_BUS.addListener(this::gatherData)
    }

    private fun clientSetup(e: FMLClientSetupEvent) {
        ScreenManager.register(Containers.REMOTE_CHEST.get(), ::RemoteChestScreen)
    }

    private fun gatherData(e: GatherDataEvent) {
        e.generator.addProvider(BlockStates(e.generator, e.existingFileHelper))
        e.generator.addProvider(LanguageEnglishUS(e.generator))
    }
}
