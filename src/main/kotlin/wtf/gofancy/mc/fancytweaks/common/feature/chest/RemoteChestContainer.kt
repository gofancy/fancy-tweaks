package wtf.gofancy.mc.fancytweaks.common.feature.chest

import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.inventory.container.Container
import net.minecraft.inventory.container.Slot
import net.minecraft.util.IIntArray
import net.minecraft.util.IWorldPosCallable
import net.minecraft.util.IntArray
import net.minecraft.util.math.BlockPos
import net.minecraft.util.text.TranslationTextComponent
import net.minecraft.world.World
import net.minecraftforge.items.IItemHandler
import net.minecraftforge.items.ItemStackHandler
import net.minecraftforge.items.SlotItemHandler
import wtf.gofancy.mc.fancytweaks.MOD_ID
import wtf.gofancy.mc.fancytweaks.Names
import wtf.gofancy.mc.fancytweaks.common.Blocks
import wtf.gofancy.mc.fancytweaks.common.Containers

internal class RemoteChestContainer(
    id: Int,
    private val level: World,
    private val pos: BlockPos,
    private val playerInventory: PlayerInventory,
    private val chestInventory: IItemHandler,
    private val data: IIntArray,
) : Container(Containers.REMOTE_CHEST.get(), id) {

    internal companion object {
        internal val TITLE = TranslationTextComponent("container.${MOD_ID}.${Names.REMOTE_CHEST}")

        // ContainerType$IFactory#create
        internal fun factory(): (Int, PlayerInventory) -> RemoteChestContainer =
            { id, inv ->
                RemoteChestContainer(
                    id,
                    inv.player.level,
                    BlockPos.ZERO,
                    inv,
                    ItemStackHandler(27),
                    IntArray(0)
                )
            }

        // IContainerProvider#createMenu
        internal fun provider(be: RemoteChestBlockEntity): (Int, PlayerInventory, PlayerEntity) -> RemoteChestContainer =
            { id, inv, _ ->
                RemoteChestContainer(
                    id,
                    inv.player.level,
                    be.blockPos,
                    inv,
                    be.inventory,
                    IntArray(1)
                )
            }
    }

    internal class RemoteChestData : IIntArray {

        private val array = kotlin.IntArray(1)

        override fun get(index: Int): Int = array[index]

        override fun set(index: Int, value: Int) {
            array[index] = value
        }

        override fun getCount(): Int = array.size

        fun getActiveUses() = this.get(0)
        fun setActiveUses(value: Int) = this.set(0, value)
    }

    private val access = IWorldPosCallable.create(this.level, this.pos)

    init {
        // Chest Inventory
        (0 until 3).forEach { row ->
            (0 until 9).forEach { column ->
                val index = column + row * 9

                val xPos = 8 + column * 18
                val yPos = 18 + row * 18

                this.addSlot(SlotItemHandler(this.chestInventory, index, xPos, yPos))
            }
        }

        // Player Inventory
        (0 until 3).forEach { row ->
            (0 until 9).forEach { column ->
                val index = column + row * 9 + 9

                val xPos = 8 + column * 18
                val yPos = 86 + row * 18

                this.addSlot(Slot(this.playerInventory, index, xPos, yPos))
            }
        }

        // Player Hotbar
        (0 until 9).forEach { column ->
            val index = column

            val xPos = 8 + column * 18
            val yPos = 144

            this.addSlot(Slot(this.playerInventory, index, xPos, yPos))
        }

        // Data
        this.addDataSlots(this.data)
    }

    override fun stillValid(player: PlayerEntity): Boolean = stillValid(this.access, player, Blocks.REMOTE_CHEST.get())
}
