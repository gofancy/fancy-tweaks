package wtf.gofancy.mc.fancytweaks.common.feature.chest

import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.SoundType
import net.minecraft.block.material.Material
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.inventory.container.SimpleNamedContainerProvider
import net.minecraft.nbt.CompoundNBT
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.ActionResultType
import net.minecraft.util.Hand
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.BlockRayTraceResult
import net.minecraft.world.IBlockReader
import net.minecraft.world.World
import net.minecraftforge.fml.network.NetworkHooks

internal class RemoteChestBlock : Block(Properties.of(Material.WOOD).strength(2.5F).sound(SoundType.WOOD)) {

    override fun hasTileEntity(state: BlockState?): Boolean = true
    override fun createTileEntity(state: BlockState?, world: IBlockReader?): TileEntity = RemoteChestBlockEntity()

    override fun use(
        state: BlockState,
        level: World,
        pos: BlockPos,
        player: PlayerEntity,
        hand: Hand,
        traceResult: BlockRayTraceResult,
    ): ActionResultType {
        val blockEntity = level.getBlockEntity(pos)

        if (blockEntity is RemoteChestBlockEntity) {
            if (player is ServerPlayerEntity) {
                if (!player.isCrouching) {
                    NetworkHooks.openGui(
                        player,
                        SimpleNamedContainerProvider(
                            RemoteChestContainer.provider(blockEntity),
                            RemoteChestContainer.TITLE,
                        )
                    )
                } else {
                    val heldStack = player.getItemInHand(hand)

                    if (heldStack.item is RemoteAccessItem) {
                        val positionNBT = CompoundNBT().apply {
                            val chestPosition = blockEntity.blockPos

                            this.putInt("x", chestPosition.x)
                            this.putInt("y", chestPosition.y)
                            this.putInt("z", chestPosition.z)
                        }

                        heldStack.orCreateTag.put("access", positionNBT)
                    }
                }
            }
            return ActionResultType.SUCCESS
        }
        return ActionResultType.PASS
    }
}
