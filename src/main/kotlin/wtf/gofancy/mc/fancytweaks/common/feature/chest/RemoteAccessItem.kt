package wtf.gofancy.mc.fancytweaks.common.feature.chest

import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemStack
import net.minecraft.item.ItemUseContext
import net.minecraft.util.ActionResult
import net.minecraft.util.ActionResultType
import net.minecraft.util.Hand
import net.minecraft.world.World

class RemoteAccessItem : Item(properties) {

    internal companion object {
        private val properties = Properties().stacksTo(1).tab(ItemGroup.TAB_MISC)

    }

    override fun useOn(p_195939_1_: ItemUseContext): ActionResultType {
        return super.useOn(p_195939_1_)
    }

    override fun use(p_77659_1_: World, p_77659_2_: PlayerEntity, p_77659_3_: Hand): ActionResult<ItemStack> {
        return super.use(p_77659_1_, p_77659_2_, p_77659_3_)
    }


}
