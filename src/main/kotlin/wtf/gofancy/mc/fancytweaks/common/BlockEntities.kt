package wtf.gofancy.mc.fancytweaks.common

import com.mojang.datafixers.types.constant.EmptyPart
import net.minecraft.tileentity.TileEntityType
import net.minecraftforge.eventbus.api.IEventBus
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries
import wtf.gofancy.mc.fancytweaks.MOD_ID
import wtf.gofancy.mc.fancytweaks.Names
import wtf.gofancy.mc.fancytweaks.common.feature.chest.RemoteChestBlockEntity

internal object BlockEntities {

    private val BLOCK_ENTITIES = DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, MOD_ID)

    internal val REMOTE_CHEST = BLOCK_ENTITIES.register(Names.REMOTE_CHEST) {
        TileEntityType.Builder.of(::RemoteChestBlockEntity, Blocks.REMOTE_CHEST.get()).build(EmptyPart())
    }

    internal fun register(bus: IEventBus) {
        BLOCK_ENTITIES.register(bus)
    }
}
