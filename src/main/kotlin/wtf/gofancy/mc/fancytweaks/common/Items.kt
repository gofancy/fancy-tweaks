package wtf.gofancy.mc.fancytweaks.common

import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraftforge.eventbus.api.IEventBus
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries
import wtf.gofancy.mc.fancytweaks.MOD_ID
import wtf.gofancy.mc.fancytweaks.Names
import wtf.gofancy.mc.fancytweaks.common.feature.chest.RemoteAccessItem

internal object Items {

    private val ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MOD_ID)

    internal val REMOTE_CHEST = ITEMS.register(Names.REMOTE_CHEST) {
        BlockItem(Blocks.REMOTE_CHEST.get(), Item.Properties().tab(ItemGroup.TAB_DECORATIONS))
    }

    internal val REMOTE_ACCESS = ITEMS.register(Names.REMOTE_ACCESS, ::RemoteAccessItem)

    internal fun register(bus: IEventBus) {
        ITEMS.register(bus)
    }
}
