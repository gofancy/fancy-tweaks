package wtf.gofancy.mc.fancytweaks.common

import net.minecraftforge.eventbus.api.IEventBus
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries
import wtf.gofancy.mc.fancytweaks.MOD_ID
import wtf.gofancy.mc.fancytweaks.Names
import wtf.gofancy.mc.fancytweaks.common.feature.chest.RemoteChestBlock

internal object Blocks {

    private val BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MOD_ID)

    internal val REMOTE_CHEST = BLOCKS.register(Names.REMOTE_CHEST) { RemoteChestBlock() }

    internal fun register(bus: IEventBus) {
        BLOCKS.register(bus)
    }
}
