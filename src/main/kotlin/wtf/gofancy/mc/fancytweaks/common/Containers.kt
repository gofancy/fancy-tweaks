package wtf.gofancy.mc.fancytweaks.common

import net.minecraft.inventory.container.ContainerType
import net.minecraftforge.eventbus.api.IEventBus
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries
import wtf.gofancy.mc.fancytweaks.MOD_ID
import wtf.gofancy.mc.fancytweaks.Names
import wtf.gofancy.mc.fancytweaks.common.feature.chest.RemoteChestContainer

internal object Containers {

    private val CONTAINERS = DeferredRegister.create(ForgeRegistries.CONTAINERS, MOD_ID)

    internal val REMOTE_CHEST = CONTAINERS.register(Names.REMOTE_CHEST) { ContainerType(RemoteChestContainer.factory()) }

    internal fun register(bus: IEventBus) {
        CONTAINERS.register(bus);
    }
}
