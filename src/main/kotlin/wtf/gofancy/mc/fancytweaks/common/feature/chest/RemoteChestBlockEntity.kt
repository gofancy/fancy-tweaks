package wtf.gofancy.mc.fancytweaks.common.feature.chest

import net.minecraft.block.BlockState
import net.minecraft.nbt.CompoundNBT
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.Direction
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.util.LazyOptional
import net.minecraftforge.items.CapabilityItemHandler
import net.minecraftforge.items.ItemStackHandler
import wtf.gofancy.mc.fancytweaks.common.BlockEntities

internal class RemoteChestBlockEntity : TileEntity(BlockEntities.REMOTE_CHEST.get()) {

    internal companion object {

        private const val NBT_INVENTORY_KEY = "inventory"
    }

    internal val inventory = object : ItemStackHandler(27) {
        override fun onContentsChanged(slot: Int) {
            super.onContentsChanged(slot)
            this@RemoteChestBlockEntity.setChanged()
        }
    }
    private val inventoryOptional = LazyOptional.of { this.inventory }

    override fun <T : Any?> getCapability(cap: Capability<T>, side: Direction?): LazyOptional<T> {
        if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return this.inventoryOptional.cast()
        }
        return super.getCapability(cap, side)
    }

    override fun invalidateCaps() {
        super.invalidateCaps()
        this.inventoryOptional.invalidate()
    }

    override fun save(nbt: CompoundNBT): CompoundNBT {
        val invCompound = this.inventory.serializeNBT()
        nbt.put(NBT_INVENTORY_KEY, invCompound)

        return super.save(nbt)
    }

    override fun load(state: BlockState, nbt: CompoundNBT) {
        super.load(state, nbt)

        val invCompound = nbt.getCompound(NBT_INVENTORY_KEY)
        this.inventory.deserializeNBT(invCompound)
    }
}
