package wtf.gofancy.mc.fancytweaks.datagen

import net.minecraft.data.DataGenerator
import net.minecraftforge.client.model.generators.ItemModelProvider
import net.minecraftforge.common.data.ExistingFileHelper
import wtf.gofancy.mc.fancytweaks.MOD_ID

internal class ItemModels(gen: DataGenerator, helper: ExistingFileHelper) : ItemModelProvider(gen, MOD_ID, helper) {

    override fun registerModels() {
        this.singleTexture()
    }
}