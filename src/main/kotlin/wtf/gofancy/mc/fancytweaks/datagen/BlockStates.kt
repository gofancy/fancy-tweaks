package wtf.gofancy.mc.fancytweaks.datagen

import net.minecraft.data.DataGenerator
import net.minecraftforge.client.model.generators.BlockStateProvider
import net.minecraftforge.common.data.ExistingFileHelper
import wtf.gofancy.mc.fancytweaks.MOD_ID
import wtf.gofancy.mc.fancytweaks.common.Blocks

internal class BlockStates(gen: DataGenerator, helper: ExistingFileHelper) : BlockStateProvider(gen, MOD_ID, helper) {

    override fun registerStatesAndModels() {
        Blocks.REMOTE_CHEST.get().let {
            simpleBlock(it)
            simpleBlockItem(it, cubeAll(it))
        }
    }
}
