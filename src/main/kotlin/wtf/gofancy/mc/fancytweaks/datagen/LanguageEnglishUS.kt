package wtf.gofancy.mc.fancytweaks.datagen

import net.minecraft.data.DataGenerator
import net.minecraftforge.common.data.LanguageProvider
import wtf.gofancy.mc.fancytweaks.MOD_ID
import wtf.gofancy.mc.fancytweaks.Names
import wtf.gofancy.mc.fancytweaks.common.Blocks

internal class LanguageEnglishUS(gen: DataGenerator) : LanguageProvider(gen, MOD_ID, "en_us") {

    override fun addTranslations() {
        add(Blocks.REMOTE_CHEST.get(), "Remote Chest")
        add("container.$MOD_ID.${Names.REMOTE_CHEST}", "Remote Chest")
    }
}
