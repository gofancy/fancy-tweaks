package wtf.gofancy.mc.fancytweaks.client.feature.chest

import com.mojang.blaze3d.matrix.MatrixStack
import net.minecraft.client.gui.screen.inventory.ContainerScreen
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.util.text.ITextComponent
import wtf.gofancy.mc.fancytweaks.Names
import wtf.gofancy.mc.fancytweaks.common.feature.chest.RemoteChestContainer
import wtf.gofancy.mc.fancytweaks.util.guiLocation

internal class RemoteChestScreen(
    container: RemoteChestContainer,
    inv: PlayerInventory,
    title: ITextComponent
) : ContainerScreen<RemoteChestContainer>(container, inv, title) {

    internal companion object {
        internal val GUI_LOCATION = guiLocation(Names.REMOTE_CHEST)
    }

    init {
        this.imageWidth = 176
        this.imageHeight = 168

        this.inventoryLabelY = this.imageHeight - 94
    }

    override fun render(stack: MatrixStack, x: Int, y: Int, partialTicks: Float) {
        this.renderBackground(stack, 0)
        super.render(stack, x, y, partialTicks)
        this.renderTooltip(stack, x, y)
    }

    override fun renderBg(stack: MatrixStack, partialTicks: Float, mouseX: Int, mouseY: Int) {
        val renderX = (this.width - this.imageWidth) / 2
        val renderY = (this.height - this.imageHeight) / 2

        this.minecraft?.getTextureManager()?.bind(GUI_LOCATION)

        this.blit(stack, renderX, renderY, 0, 0, this.imageWidth, this.imageHeight)
    }
}
