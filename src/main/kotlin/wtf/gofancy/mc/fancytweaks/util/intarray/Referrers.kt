package wtf.gofancy.mc.fancytweaks.util.intarray

import kotlin.reflect.KMutableProperty

interface Referrer<T> {

    fun get(): T
    fun set(value: T)
}

class PropertyReferrer<T>(private val property: KMutableProperty<T>) : Referrer<T> {

    override fun get(): T = this.property.getter.call()
    override fun set(value: T) = this.property.setter.call(value)
}

class FunctionReferrer<T>(private val getter: () -> T, private val setter: (T) -> Unit) : Referrer<T> {
    override fun get(): T = this.getter.invoke()
    override fun set(value: T) = this.setter.invoke(value)
}

class ValueReferrer<T>(initial: T) : Referrer<T> {

    private var value = initial
    override fun get(): T = this.value
    override fun set(value: T) {
        this.value = value
    }
}
