package wtf.gofancy.mc.fancytweaks.util.intarray

import net.minecraft.util.IIntArray
import kotlin.reflect.KMutableProperty

class AssociativeIntArray private constructor(private val internal: Map<String, Referrer<Int>>) : IIntArray {

    companion object {

        fun make(block: Builder.() -> Unit) = Builder().apply(block).build()
        fun of(vararg keys: String) = make {
            keys.forEach { it(0) }
        }
    }

    class Builder {

        private val entries = mutableMapOf<String, Referrer<Int>>()

        operator fun String.invoke(initial: Int) {
            this@Builder.entries[this] = ValueReferrer(initial)
        }

        operator fun String.invoke(property: KMutableProperty<Int>) {
            this@Builder.entries[this] = PropertyReferrer(property)
        }

        operator fun String.invoke(getter: () -> Int, setter: (Int) -> Unit) {
            this@Builder.entries[this] = FunctionReferrer(getter, setter)
        }

        fun build() = AssociativeIntArray(this.entries.toMap())
    }

    private val size = this.internal.size

    override operator fun get(index: Int): Int = this[this.getKey(index)]
    operator fun get(key: String): Int = this.internal[key]?.get() ?: throw NoSuchElementException()

    override operator fun set(index: Int, value: Int) = this.set(this.getKey(index), value)
    operator fun set(key: String, value: Int) = this.internal[key]?.set(value) ?: throw NoSuchElementException()

    override fun getCount(): Int = this.size

    private fun getKey(index: Int) = this.internal.keys.toList().getOrNull(index) ?: throw IndexOutOfBoundsException()
}
