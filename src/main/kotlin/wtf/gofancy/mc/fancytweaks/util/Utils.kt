package wtf.gofancy.mc.fancytweaks.util

import net.minecraft.util.ResourceLocation
import wtf.gofancy.mc.fancytweaks.MOD_ID

internal fun guiLocation(name: String) = rl(MOD_ID, "textures/gui/$name.png")

internal fun rl(namespace: String, path: String) = ResourceLocation(namespace, path)
