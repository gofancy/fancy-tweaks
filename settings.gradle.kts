pluginManagement {
    repositories {
        gradlePluginPortal()
        maven {
            name = "forge"
            url = uri("https://maven.minecraftforge.net")
        }
    }
    resolutionStrategy {
        eachPlugin {
            if (requested.id.toString() == "net.minecraftforge.gradle") {
                useModule("${requested.id}:ForgeGradle:${requested.version}")
            }
        }
    }
}

rootProject.name = "fancytweaks"
