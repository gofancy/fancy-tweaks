import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import net.minecraftforge.gradle.common.util.RunConfig
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.time.Instant
import java.time.format.DateTimeFormatter

plugins {
    kotlin("jvm") version "1.4.32"

    id("net.minecraftforge.gradle") version "4.1.9"
    id("com.github.johnrengelman.shadow") version "6.1.0"
}

version = "0.0.1"
group = "wtf.gofancy"

val modid: String = name.toLowerCase()

base {
    archivesBaseName = modid
}

minecraft {
    mappings("official", "1.16.5")

    runs {
        createWithDefaults("client")
        createWithDefaults("server")
        createWithDefaults("data") {
            property("forge.logging.console.level", "debug")

            args(
                "--mod",
                modid,
                "--all",
                "--output",
                project.file("src/generated/resources/"),
                "--existing",
                project.file("src/main/resources/")
            )
        }
    }
}

sourceSets.main {
    resources {
        srcDir("src/generated/resources")
    }
}

reobf {
    create("shadowJar")
}

repositories {
    mavenCentral()
    maven {
        name = "gofancy logger"
        url = uri("https://gitlab.com/api/v4/projects/25660282/packages/maven")
    }
    maven {
        name = "kotlin for forge"
        url = uri("https://thedarkcolour.github.io/KotlinForForge/")
    }
}

val bundled: Configuration by configurations.creating

configurations {
    implementation.get().extendsFrom(bundled)
}

dependencies {
    minecraft(group = "net.minecraftforge", name = "forge", version = "1.16.5-36.1.16")

    fg.deobf(implementation(group = "thedarkcolour", name = "kotlinforforge", version = "1.11.0"))

    bundled(group = "wtf.gofancy", name = "logger", version = "2.0.2")
    bundled(group = "org.apache.logging.log4j", name = "log4j-slf4j18-impl", version = "2.14.1")
}

tasks {
    withType<Jar> {
        manifest {
            attributes(
                "Name" to "${project.group.toString().replace(".", "/")}/${
                    project.name.toLowerCase().replace(" ", "_")
                }/",
                "Specification-Title" to project.name,
                "Specification-Version" to project.version,
                "Specification-Vendor" to "Garden of Fancy",
                "Implementation-Title" to "${project.group}.${project.name.toLowerCase().replace(" ", "_")}",
                "Implementation-Version" to project.version,
                "Implementation-Vendor" to "Garden of Fancy",
                "Implementation-Timestamp" to DateTimeFormatter.ISO_INSTANT.format(Instant.now())
            )
        }

        archiveAppendix.set("1.16.5")

        finalizedBy("reobfJar")
    }

    withType<ShadowJar> {
        configurations = listOf(bundled)

        archiveClassifier.set("bundled")

        finalizedBy("reobfShadowJar")
    }

    withType<JavaCompile> {
        sourceCompatibility = JavaVersion.VERSION_1_8.toString()
        targetCompatibility = JavaVersion.VERSION_1_8.toString()
    }

    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_1_8.toString()

            languageVersion = "1.4"
            apiVersion = "1.4"
        }
    }

    withType<Wrapper> {
        gradleVersion = "6.8.3"
        distributionType = Wrapper.DistributionType.ALL
    }
}

fun NamedDomainObjectContainerScope<RunConfig>.createWithDefaults(
    name: String,
    configuration: RunConfig.() -> Unit = {}
) {
    if (name.isNotBlank()) {
        this.create(name) {
            workingDirectory(project.file(taskName))

            // SCAN REGISTRIES REGISTRYDUMP
            property("forge.logging.markers", "REGISTRIES")
            property("forge.logging.console.level", "warn")

            mods {
                create(modid) {
                    source(project.sourceSets.main.get())
                }
            }
        }.apply(configuration)
    }
}
